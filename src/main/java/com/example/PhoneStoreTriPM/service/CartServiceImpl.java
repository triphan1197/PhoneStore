package com.example.PhoneStoreTriPM.service;

import com.example.PhoneStoreTriPM.dto.ItemCartForCreate;
import com.example.PhoneStoreTriPM.dto.ProductForCreate;
import com.example.PhoneStoreTriPM.dto.ProductForUpdate;
import com.example.PhoneStoreTriPM.entity.CartItem;
import com.example.PhoneStoreTriPM.entity.Product;
import com.example.PhoneStoreTriPM.repository.CartItemRepository;
import com.example.PhoneStoreTriPM.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartItemRepository cartItemRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<CartItem> getAll() {
        return cartItemRepository.findAll();
    }

    @Override
    @Transactional
    public void addToCart(ItemCartForCreate itemCartDTO) {
        Long productId = itemCartDTO.getProduct().getId();
        int quantity = itemCartDTO.getQuantity();

        Optional<Product> productOptional = productRepository.findById(productId);
        if (productOptional.isPresent()) {
            Product product = productOptional.get();
            CartItem existingCartItem = cartItemRepository.findByProduct(product);
            if (existingCartItem != null) {
                existingCartItem.setQuantity(existingCartItem.getQuantity() + quantity);
                cartItemRepository.save(existingCartItem);
            } else {
                CartItem cartItem = new CartItem();
                cartItem.setProduct(product);
                cartItem.setQuantity(quantity);
                cartItemRepository.save(cartItem);
            }
        }
    }

    @Override
    @Transactional
    public void removeFromCart(Long productId) {
        Product product = productRepository.findById(productId).orElse(null);
        if (product != null) {
            CartItem existingCartItem = cartItemRepository.findByProduct(product);
            if (existingCartItem != null) {
                cartItemRepository.delete(existingCartItem);
            }
        }
    }

    @Override
    @Transactional
    public void clearCart() {
        cartItemRepository.deleteAll();
    }

    @Override
    public void save(ItemCartForCreate itemCartDTO) {

    }
}





