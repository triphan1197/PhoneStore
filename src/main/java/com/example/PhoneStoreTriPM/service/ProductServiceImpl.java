package com.example.PhoneStoreTriPM.service;

import com.example.PhoneStoreTriPM.dto.ProductForCreate;
import com.example.PhoneStoreTriPM.dto.ProductForUpdate;
import com.example.PhoneStoreTriPM.entity.Product;
import com.example.PhoneStoreTriPM.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository ;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public List<Product> getAll() {
        return productRepository.findAll();
    }

    @Override
    @Transactional
    public void save(ProductForCreate productDTO) {
        Product theProduct = modelMapper.map(productDTO, Product.class);
        productRepository.save(theProduct);
    }

    @Override
    @Transactional
    public void update(ProductForUpdate productDTO) {
        Product theProduct = modelMapper.map(productDTO, Product.class);
        productRepository.save(theProduct);
    }

    @Override
    @Transactional
    public ProductForUpdate getById(Long id) {

        Optional<Product> product = productRepository.findById(id);
        return product.map(value -> modelMapper.map(value, ProductForUpdate.class)).orElse(null);
    }
}
