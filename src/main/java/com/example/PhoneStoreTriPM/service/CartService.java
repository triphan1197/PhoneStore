package com.example.PhoneStoreTriPM.service;

import com.example.PhoneStoreTriPM.dto.ItemCartForCreate;

import com.example.PhoneStoreTriPM.entity.CartItem;

import java.util.List;

public interface CartService {
    List<CartItem> getAll();

    void addToCart(ItemCartForCreate itemCartDTO);

    void removeFromCart(Long productId);

    void clearCart();

    void save(ItemCartForCreate itemCartDTO);
}
