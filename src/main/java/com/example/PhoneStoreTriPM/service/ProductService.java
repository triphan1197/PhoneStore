package com.example.PhoneStoreTriPM.service;

import com.example.PhoneStoreTriPM.dto.ProductForCreate;
import com.example.PhoneStoreTriPM.dto.ProductForUpdate;
import com.example.PhoneStoreTriPM.entity.Product;

import java.util.List;

public interface ProductService {
    public List<Product> getAll();

    public void save(ProductForCreate productDTO);

    public void update(ProductForUpdate productDTO);

    public ProductForUpdate getById(Long id);

//    public void deleteById(Long id);
}
