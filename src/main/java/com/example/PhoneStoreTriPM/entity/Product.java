package com.example.PhoneStoreTriPM.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private Long itemcode;
    @Column
    private String manufacturer;
    @Column
    private String category;
    @Column
    private String image;
    @Column
    private Long unit;
    @Column
    private Long price;

    public Product(Long id) {
        this.id = id;
    }



}
