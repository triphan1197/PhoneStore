package com.example.PhoneStoreTriPM.repository;

import com.example.PhoneStoreTriPM.entity.CartItem;
import com.example.PhoneStoreTriPM.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Long> {
    CartItem findByProduct(Product product);
}
