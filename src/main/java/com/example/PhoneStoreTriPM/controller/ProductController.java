package com.example.PhoneStoreTriPM.controller;

import com.example.PhoneStoreTriPM.dto.ProductForCreate;
import com.example.PhoneStoreTriPM.dto.ProductForUpdate;
import com.example.PhoneStoreTriPM.entity.Product;
import com.example.PhoneStoreTriPM.service.CartService;
import com.example.PhoneStoreTriPM.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping("/list")
    public String list(Model theModel) {
        List<Product> theProducts = productService.getAll();
        theModel.addAttribute("products", theProducts);
        return "home";
    }
    @GetMapping("/add")
    public String addForm(Model theModel) {
        ProductForCreate productDTO = new ProductForCreate();
        theModel.addAttribute("product", productDTO);
        theModel.addAttribute("successMessage", "successfully!");
        return "product-add-form";
    }
    @PostMapping("/add")
    public String addDatabase(@ModelAttribute("product") ProductForCreate productDTO) {
        productService.save(productDTO);
        return "redirect:/product/list";
    }
//    @GetMapping("/update/{id}")
//    public String updateForm(@PathVariable("id") Long productId,
//                             Model theModel) {
//        ProductForUpdate productDTO = productService.getById(productId);
//        theModel.addAttribute("product", productDTO);
//        return "product-update-form";
//    }
//    @PostMapping("/update")
//    public String updateDatabase(@ModelAttribute("product") ProductForUpdate productDTO) {
//        productService.update(productDTO);
//        return "redirect:/product/list";
//    }
@GetMapping("/detail/{id}")
public String detail(@PathVariable Long id, Model theModel) {
    ProductForUpdate productDTO = productService.getById(id);
    theModel.addAttribute("product", productDTO);
    return "detail-product";
}


}
