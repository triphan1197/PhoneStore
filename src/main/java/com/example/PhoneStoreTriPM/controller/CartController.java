package com.example.PhoneStoreTriPM.controller;

import com.example.PhoneStoreTriPM.dto.ItemCartForCreate;
import com.example.PhoneStoreTriPM.entity.CartItem;

import com.example.PhoneStoreTriPM.entity.Product;
import com.example.PhoneStoreTriPM.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@Controller
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @GetMapping("/view")
    public String viewCart(Model model) {
        List<CartItem> cartItems = cartService.getAll();
        model.addAttribute("cartItems", cartItems);
        return "cart";
    }

    @PostMapping("/add/{productId}")
    public String addToCart(@PathVariable Long productId, @RequestParam(defaultValue = "1") int quantity) {
        ItemCartForCreate itemCartDTO = new ItemCartForCreate();
        itemCartDTO.setProduct(new Product(productId));
        itemCartDTO.setQuantity(quantity);

        cartService.addToCart(itemCartDTO);

        return "redirect:/cart/view";
    }

    @PostMapping("/remove/{productId}")
    public String removeFromCart(@PathVariable Long productId) {
        cartService.removeFromCart(productId);
        return "redirect:/cart/view";
    }

    @PostMapping("/clear")
    public String clearCart() {
        cartService.clearCart();
        return "redirect:/cart/view";
    }
}
