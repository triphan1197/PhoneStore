package com.example.PhoneStoreTriPM.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController {

    @GetMapping(value = { "/", "/home" })
    public String homepage() {
        return "hello";
    }

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }
//    @GetMapping("/addproduct")
//    public String addproduct() {
//        return "product-add-form";
//    }

}
