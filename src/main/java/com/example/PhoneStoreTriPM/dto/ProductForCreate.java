package com.example.PhoneStoreTriPM.dto;

import lombok.*;

import javax.persistence.Column;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ProductForCreate {

    private String name;

    private String description;

    private Long itemcode;

    private String manufacturer;

    private String category;

    private Long unit;

    private Long price;

    private String image;
}
