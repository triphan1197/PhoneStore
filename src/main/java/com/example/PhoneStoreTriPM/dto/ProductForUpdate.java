package com.example.PhoneStoreTriPM.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ProductForUpdate {
    private Long id;
    private String name;

    private String description;

    private Long itemcode;

    private String manufacturer;

    private String category;

    private Long unit;

    private Long price;
    private String image;
}
