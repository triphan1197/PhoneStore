package com.example.PhoneStoreTriPM.dto;

import com.example.PhoneStoreTriPM.entity.Product;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ItemCartForCreate {

    private Product product;

    private int quantity;


}
