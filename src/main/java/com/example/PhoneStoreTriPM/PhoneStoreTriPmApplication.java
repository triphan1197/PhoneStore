package com.example.PhoneStoreTriPM;

import com.example.PhoneStoreTriPM.entity.User;
import com.example.PhoneStoreTriPM.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class PhoneStoreTriPmApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(PhoneStoreTriPmApplication.class, args);
	}

	@Autowired
	UserRepository userRepository;
	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public void run(String... args) throws Exception {
//				 Khi chương trình chạy
////		 Insert vào csdl một user.
//		User user = new User();
//		user.setUsername("tri1102");
//		user.setPassword(passwordEncoder.encode("tri1102"));
//		userRepository.save(user);
//		System.out.println(user);
	}
}
